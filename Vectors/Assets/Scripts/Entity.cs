﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class Entity : MonoBehaviour
    {

        private Vector3 m_position;
        private Vector3 m_velocity;
        private Vector3 m_acceleration;

        private bool hasCrossed = false;

        public Entity()
        {
            m_position = new Vector3(0.0f, 0.0f, 0.0f);
            m_velocity = Vector3.zero;
            m_acceleration = Vector3.zero;
           
        }

        private void UpdatePosition()
        {
            m_position = m_position + m_velocity;
        }

        private void UpdateVelocity()
        {
            m_velocity += m_acceleration;

            UpdatePosition();
        }

        public void UpdateAcceleration(Vector3 _value)
        {
            m_acceleration += _value;
            UpdateVelocity();
        }
        public Vector3 GetPosition()
        {
            if(m_position.x >= 400)
            {
                hasCrossed = true;
                m_velocity.x = -0.001f;
                m_acceleration.x = -0.001f;
                m_position.x = 399.999f;
            }

            if (m_position.x <= 0)
            {
                hasCrossed = false;
                m_velocity.x = 0.001f;
                m_acceleration.x = 0.001f;
                m_position.x = 0.001f;
            }

            return m_position;
        }

        public bool GetCrossed()
        {
            if(hasCrossed == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        

    }
}