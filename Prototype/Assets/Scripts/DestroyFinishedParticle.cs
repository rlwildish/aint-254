﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class DestroyFinishedParticle : MonoBehaviour
    {

        private ParticleSystem thisParticleSystem;

        // Use this for initialization
        void Start()
        {

            thisParticleSystem = GetComponent<ParticleSystem>();

        }

        // Update is called once per frame
        void Update()
        {
            //if the paricle is still playing exit this loop otherwise destroy the particle
            if (thisParticleSystem.isPlaying)
            {
                return;
            }

            Destroy(gameObject);

        }
    }
}
