﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class HurtEnemyOnStomp : MonoBehaviour
    {

        public int damageToGive = 3;

        public float bounceOnEnemy = 10;

        private Rigidbody2D rb2d;

        public GameObject stompScooter;

        public GameObject stompPogo;

        // Use this for initialization
        void Start()
        {

            rb2d = transform.parent.GetComponent<Rigidbody2D>();

        }

        // Update is called once per frame
        void Update()
        {
            //This sets the stomp for the player so that the player should only be able to stomp on enemies with the stomp at the bottom of the player (as the player switches localScale on the y axis, this changes)
            if(PlayerBehaviour.GetIsScooter() == false)
            {
                stompScooter.SetActive(false);
                stompPogo.SetActive(true);
            }
            else
            {
                stompScooter.SetActive(true);
                stompPogo.SetActive(false);
            }


        }
        //on entering collider if the tag of the object is Enemy or Boss use the healthManager of the respective object to damage it and make the player bounce off the object
        void OnTriggerEnter2D(Collider2D other)
        {

            if (other.tag == "Enemy")
            {
                other.GetComponent<EnemyHealhtManager>().giveDamage(damageToGive);
                rb2d.velocity = new Vector2(rb2d.velocity.x, bounceOnEnemy);
            }

            if(other.tag == "Boss")
            {

                other.GetComponent<BossHealthManager>().giveDamage(damageToGive);
                rb2d.velocity = new Vector2(rb2d.velocity.x, bounceOnEnemy);

            }



        }
    }
}