﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class Bullet : MonoBehaviour
    {
        private Rigidbody m_rigidbody;

        [SerializeField]
        private float m_force = 500;
        
        // Use this for initialization
        void Start()
        {
            m_rigidbody = transform.GetComponent<Rigidbody>();
            m_rigidbody.AddForce(transform.forward * m_force, ForceMode.Impulse);
            Destroy(gameObject, 5.0f);
        }
    }
}
