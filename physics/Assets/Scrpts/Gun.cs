﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class Gun : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_bullet;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if(Input.GetMouseButtonDown(0))
            {
                Instantiate(m_bullet, transform.position + transform.forward * 3.0f, transform.rotation);
            }
        }
    }
}