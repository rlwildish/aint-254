﻿using UnityEngine;
using System.Collections;
namespace ISS
{
    public class Movement : MonoBehaviour
    {

        [SerializeField]
        private float m_speed = 0.0000001f;

        [SerializeField]
        private float m_rotationSpeed = 250;

        private Transform m_transform;

        private float rotationY;
        private float rot;
        // Use this for initialization
        void Start()
        {

            m_transform = transform;
        }

        // Update is called once per frame
        void Update()
        {

            rotationY = transform.rotation.eulerAngles.y;

            rot = m_rotationSpeed * Input.GetAxis("Horizontal") * Time.deltaTime;

            m_transform.rotation = Quaternion.Euler(new Vector3(0, rotationY + rot, 0));

            float vertical = Input.GetAxis("Vertical");

            float posX = m_transform.position.x;

            float posY = m_transform.position.y;

            m_transform.transform.position += new Vector3(posX, posY, vertical * m_speed);

            // transform.position = Quaternion.Euler(new Vector3());
            //if (Input.GetKeyDown("W"))
            //{

            //}
            //if (Input.GetKeyDown(Input.))
            //{
            //    m_transform.Rotate(new Vector3(0, 1, 0) * m_rotationSpeed * Time.deltaTime);
            //}
            //if (Input.GetKeyDown("S"))
            //{
            //    m_transform.Rotate(new Vector3(0, -1, 0) * m_rotationSpeed * Time.deltaTime);
            //}
            //if (Input.GetKeyDown("D"))
            //{

            //}
        }

        public float GetRotationY( float rotY)
        {
            float rotation = rotationY + rot;

            return rotation;
        }
    }
}
