﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class Follow : MonoBehaviour
    {
        private Transform m_transform;
        private Vector3 pointWorld;

        // Use this for initialization
        void Start()
        {
            m_transform = transform;
        }

        void OnDrawGizmosSelected()
        {
            pointWorld = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

            Gizmos.color = Color.red;

            Gizmos.DrawSphere(pointWorld, 0.1f);
            Debug.Log("hello");

            Mathf.Atan2(pointWorld.x, pointWorld.y);
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 cubePoint = m_transform.position;

            Vector3 rotate = Vector3.Normalize(cubePoint - pointWorld);

            float angle = Mathf.Atan2(rotate.x, rotate.z) * Mathf.Rad2Deg;

            m_transform.rotation = Quaternion.Euler(0, angle, 0);
        }
    }
}