﻿using UnityEngine;
using System.Collections;

public class EnemyLeftToRight : MonoBehaviour {

    public float moveSpeed = 3;

    public bool moveRight;

    public Vector2 startPosition;

    

    // Use this for initialization
    void Start()
    {
        startPosition = new Vector2(GetComponent<Rigidbody2D>().position.x, GetComponent<Rigidbody2D>().position.y);
    }

    // Update is called once per frame
    void Update()
    {

        

        if (!moveRight)
        {
            //if the position (x co-ordinate) of this object is less than the startPosition - 7 start moving the other direction
            if (GetComponent<Rigidbody2D>().position.x <= startPosition.x - 7)
            {
                moveRight = !moveRight;
            }
        }

        if (moveRight)
        {
            //if the position (x co-ordinate) of this object is more than the startPosition + 7 start moving the other direction
            if (GetComponent<Rigidbody2D>().position.x >= startPosition.x + 7)
            {
                moveRight = !moveRight;
            }
        }
        //change the direction the graphic is facing dependant on which direction the enemy is travelling in, also allow for the change in direction
        if (moveRight)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }

    }
}
