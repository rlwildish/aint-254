﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RyanWildish
{
    public class UI : MonoBehaviour
    {

        public static int score = 0;

        public Text scoreText;

        private LifeManager lifeManager;

        void Start()
        {
            lifeManager = FindObjectOfType<LifeManager>();
        }

        // Update is called once per frame
        void Update()
        {
            //If the score falls below 0 (because the player died so much etc.) set the score to 0
            if (score < 0)
            {
                score = 0;
            }
            //If the score reaches or surpasses 500 then take 500 score away and give the player an extra life
            if (score >= 500)
            {
                score -= 500;
                lifeManager.GiveLife();
            }
            //display text 
            scoreText.text = ("Score: " + score);

        }
        //static function to be called from other scripts to allow the points to be added to the score
        public static void AddPoints(int pointsToAdd)
        {
            score += pointsToAdd;
        }
        
    }
}