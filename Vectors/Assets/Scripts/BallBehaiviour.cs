﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class BallBehaiviour : MonoBehaviour
    {
        private Entity m_entity;
        private Transform m_transform;
        private bool hasCrossed = false;


        // Use this for initialization
        void Start()
        {
            m_entity = new Entity();
            m_transform = transform;
        }

        // Update is called once per frame
        void Update()
        {
            hasCrossed = m_entity.GetCrossed();
            if (hasCrossed == false)
            {
                m_entity.UpdateAcceleration(new Vector3(0.0001f, 0.0f, 0.0f));
            }
            if (hasCrossed == true)
            {
                m_entity.UpdateAcceleration(new Vector3(-0.0001f, 0.0f, 0.0f));
            }
            m_transform.position = m_entity.GetPosition();
            //transform.position = new Vector3(Mathf.PingPong(Time.time, 800), transform.position.y, transform.position.z);
        }

    }
}
