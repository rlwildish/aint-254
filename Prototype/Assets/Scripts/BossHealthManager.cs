﻿using UnityEngine;
using System.Collections;

namespace RyanWildish { 

public class BossHealthManager : MonoBehaviour {

    private int enemyHealth = 9;

    public GameObject deathEffect;

    public int pointsOnDeath;

        public GameObject bossPrefab;

        private float minSize;

    // Use this for initialization
    void Start()
    {
            minSize = 1f;
    }

    // Update is called once per frame
    void Update()
    {
            //If the enemies health is equal to or less than 0 
        if (enemyHealth <= 0)
        {
                //create a prefab at the boss' position and rotation
            Instantiate(deathEffect, transform.position, transform.rotation);
                //add points to players score
            UI.AddPoints(pointsOnDeath);
                //if the localScale.y is more than minSize
            if (transform.localScale.y > minSize)
                {
                    //Create these 2 GameObjects, using the prefab, position and rotation provided passed as a GameObject so that Unity knows we are creating GameObjects with these values and prefab
                    GameObject clone1 = Instantiate(bossPrefab, new Vector3(transform.position.x + 1, transform.position.y, transform.position.z), transform.rotation) as GameObject;
                    GameObject clone2 = Instantiate(bossPrefab, new Vector3(transform.position.x - 1, transform.position.y, transform.position.z), transform.rotation) as GameObject;

                    //set the x and y localScale values to half and set the health value to the specified
                    clone1.transform.localScale = new Vector3(transform.localScale.x * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z);
                    clone1.GetComponent<BossHealthManager>().enemyHealth = 3;
                    clone2.transform.localScale = new Vector3(transform.localScale.x * 0.5f, transform.localScale.y * 0.5f, transform.localScale.z);
                    clone2.GetComponent<BossHealthManager>().enemyHealth = 3;

                }
            
            //Destroy the boss that has just been killed
            Destroy(gameObject);
        }



    }
        //method to allow player to damege this enemy
    public void giveDamage(int damageToGive)
    {

        enemyHealth -= damageToGive;

    }
}
}
