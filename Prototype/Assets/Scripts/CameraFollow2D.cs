﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class CameraFollow2D : MonoBehaviour
    {

        public PlayerBehaviour player;

        public bool isFollowing;

        public float xOffset;
        public float yOffset;


        void Start()
        {
            //player equals a gameObject with this script on it
            player = FindObjectOfType<PlayerBehaviour>();

            isFollowing = true;
        }
        // Update is called once per frame
        void Update()
        {
            if (isFollowing)
            {
                //this objects transform.position is set to equal the players transform.position with the offSets specified
                transform.position = new Vector3(player.transform.position.x + xOffset, player.transform.position.y + yOffset, transform.position.z);
            }
        }
    }
}
