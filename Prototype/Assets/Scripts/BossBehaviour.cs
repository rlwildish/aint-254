﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{


    public class BossBehaviour : MonoBehaviour
    {
        
        public float moveSpeed = 3;

        public static bool moveRight;

        public Transform wallCheck;
        public float wallCheckRadius;
        public LayerMask whatIsWall;
        private bool isHittingWall;
        private bool isGrounded;
        //public Transform groundPoint;
        public float groundRadius = 0.01f;
        private bool isNotAtEdge;
        public Transform edgeCheck;

        private float ySize;

        public GameObject bullet;
        public float shootTimer = 3;
        public GameObject gunBarrel;
        private Rigidbody2D jump;
        public float jumpTimer = 1.5f;

        // Use this for initialization
        void Start()
        {
            //ySize is used to allow the "clones" to reduce their size by half of the boss that was just killed
            //If this is not used we will only ever get half of the original sizer of the boss when using transform.localScale with an x value of 1 and -1
            ySize = transform.localScale.y;
            moveRight = false;
            jump = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        void Update()
        {
            //return true or false dependant on whether the position (with designated radius) is touching the other object (a specified layer in this case)
            isGrounded = Physics2D.OverlapCircle(edgeCheck.position, groundRadius, whatIsWall);
            //return true or false dependant on whether the position (with designated radius) is touching the other object (a specified layer in this case)
            isHittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
            //return true or false dependant on whether the position (with designated radius) is touching the other object (a specified layer in this case)
            isNotAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);
            //if the object is touching the floor and either hitting a wall or about to go off an edge, move in the other direction
            if(isGrounded)
            if (isHittingWall || !isNotAtEdge)
            {
                moveRight = !moveRight;
            }
            //flip character graphic around so the character looks like its going forward.  Change direction of movement based on which way the character should be going
            if (moveRight)
            {
                transform.localScale = new Vector3(-ySize, transform.localScale.y, transform.localScale.z);
                GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                transform.localScale = new Vector3(ySize, transform.localScale.y, transform.localScale.z);
                GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            }
            //Call shoot function if timer is equal to or less than 0 and set the timer randomly between 1 and 5. This stops the clones shooting at the exact same time
            if (shootTimer <= 0) 
            {
                Shoot();
                shootTimer = Random.Range(1, 5);
            }
            //if the jump timer is less than or equal to 0 add force to the object in respective x and y co-ordinates, set the timer randomly between 1 and 5. This stops the clones jumping at the exact same time
            if (jumpTimer <= 0)
            {
                jump.AddForce(new Vector2(0, 1500));
                jumpTimer = Random.Range(1, 5);
                
            }
            //jump and shoot timer decrease based on real time within the game. If timeScale is set to a lesser or higher value the timer will decrease slower or faster, respectively.
            jumpTimer -= Time.deltaTime;

            shootTimer -= Time.deltaTime;

        }
        //Create a bullet at gameObjects position and rotation
        private void Shoot()
        {

            Debug.Log("Shoot");

            Instantiate(bullet, gunBarrel.transform.position, gunBarrel.transform.rotation);

            shootTimer = 3;



        }
        //Static Getter to allow other scripts to gain access to a private variable. This is used to allow the bullet to know which direction to travel in
        public static bool GetMoveRight()
        {

            return moveRight;

        }

    }
}