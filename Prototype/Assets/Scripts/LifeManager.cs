﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RyanWildish
{

    public class LifeManager : MonoBehaviour
    {

        //public int startingLives;
        private int lifeCounter;

        private Text text;

        private float timer = 1.4f;

        // Use this for initialization
        void Start()
        {
            text = GetComponent<Text>();
            lifeCounter = PlayerPrefs.GetInt("PlayerCurrentLives");
        }

        // Update is called once per frame
        void Update()
        {
            text.text = "x " + lifeCounter;
            
            if (lifeCounter < 0)
                

            if (lifeCounter < 0)
            {
                timer -= Time.deltaTime;
                //stops lives from reading as -1 on gameOver
                text.text = "x 0";
                //Sets current level (player prefs variable) to 0 so if the player clicks continue on startMenu, they will stay on startMenu as the gameOver state should remove their progress
                PlayerPrefs.SetInt("CurrentLevel", 0);
                //when the timer reaches 0 load gameOver scene
                if(timer <= 0)
                Application.LoadLevel(1);
            }
        }
        //function to give a life to the player, sets playerPrefs value as well so if the player quits they have the same amount of lives no matter what level they go to
        public void GiveLife()
        {
            lifeCounter++;
            PlayerPrefs.SetInt("PlayerCurrentLives", lifeCounter);
        }
        //function to take a life from the player, sets playerPrefs value as well so if the player quits they have the same amount of lives no matter what level they go to
        public void TakeLife()
        {
            lifeCounter--;
            PlayerPrefs.SetInt("PlayerCurrentLives", lifeCounter);
        }
    }
}