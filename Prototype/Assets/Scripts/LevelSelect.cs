﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelect : MonoBehaviour {

    public Button oneOne;
    public Button oneTwo;
    public Button oneThree;
    public Button oneFour;
    public Button oneFive;
    public Button twoOne;
    public Button twoTwo;
    public Button twoThree;
    public Button twoFour;
    public Button twoFive;
    public Button threeOne;
    public Button threeTwo;
    public Button threeThree;
    public Button threeFour;
    public Button threeFive;
    public Button fourOne;
    public Button fourTwo;
    public Button fourThree;
    public Button fourFour;
    public Button fourFive;
    public Button fiveOne;
    public Button fiveTwo;
    public Button fiveThree;
    public Button fiveFour;
    public Button fiveFive;


    // Use this for initialization
    void Start () {

        oneOne = oneOne.GetComponent<Button>();
        oneTwo = oneOne.GetComponent<Button>();
        oneThree = oneOne.GetComponent<Button>();
        oneFour = oneOne.GetComponent<Button>();
        oneFive = oneOne.GetComponent<Button>();
        twoOne = oneOne.GetComponent<Button>();
        twoTwo = oneOne.GetComponent<Button>();
        twoThree = oneOne.GetComponent<Button>();
        twoFour = oneOne.GetComponent<Button>();
        twoFive = oneOne.GetComponent<Button>();
        threeOne = oneOne.GetComponent<Button>();
        threeTwo = oneOne.GetComponent<Button>();
        threeThree = oneOne.GetComponent<Button>();
        threeFour = oneOne.GetComponent<Button>();
        threeFive = oneOne.GetComponent<Button>();
        fourOne = oneOne.GetComponent<Button>();
        fourTwo = oneOne.GetComponent<Button>();
        fourThree = oneOne.GetComponent<Button>();
        fourFour = oneOne.GetComponent<Button>();
        fourFive = oneOne.GetComponent<Button>();
        fiveOne = oneOne.GetComponent<Button>();
        fiveTwo = oneOne.GetComponent<Button>();
        fiveThree = oneOne.GetComponent<Button>();
        fiveFour = oneOne.GetComponent<Button>();
        fiveFive = oneOne.GetComponent<Button>();

    }
	
	// Update is called once per frame
	void Update () {
	
	}
    //load level based on button pressed
    public void OneOne()
    {
        Application.LoadLevel(3);
    }

    public void OneTwo()
    {
        Application.LoadLevel(4);
    }

    public void OneThree()
    {
        Application.LoadLevel(5);
    }

    public void OneFour()
    {
        Application.LoadLevel(6);
    }

    public void OneFive()
    {
        Application.LoadLevel(7);
    }

    public void TwoOne()
    {
        Application.LoadLevel(8);
    }

    public void TwoTwo()
    {
        Application.LoadLevel(9);
    }

    public void TwoThree()
    {
        Application.LoadLevel(10);
    }

    public void TwoFour()
    {
        Application.LoadLevel(11);
    }

    public void TwoFive()
    {
        Application.LoadLevel(12);
    }
}
