﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class EnemyHealhtManager : MonoBehaviour
    {

        public int enemyHealth = 3;

        public GameObject deathEffect;

        public int pointsOnDeath;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //if the enemy has 0 or less health, Instantiate a death particle at its current location, add points to the players score and destroy the enemy object
            if (enemyHealth <= 0)
            {
                Instantiate(deathEffect, transform.position, transform.rotation);
                UI.AddPoints(pointsOnDeath);
                Destroy(gameObject);
            }

        }

        public void giveDamage(int damageToGive)
        {

            enemyHealth -= damageToGive;

        }
    }
}