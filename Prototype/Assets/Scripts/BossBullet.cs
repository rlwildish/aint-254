﻿using UnityEngine;
using System.Collections;
namespace RyanWildish
{

    public class BossBullet : MonoBehaviour
    {

        public static bool ismoveRight;
        private float moveSpeed = 0.5f;
        private float destroyTimer = 2f;
        private static int damageToGive = 1;

        public Vector2 playerPos;

        // Use this for initialization
        void Start()
        {
            //uses a getter method to gain access to a private variable
            ismoveRight = BossBehaviour.GetMoveRight();

        }

        // Update is called once per frame
        void Update()
        {


            //movement, changes local scale if travelling left, to show a good graphical representation of which direction the bullet is travelling
            if (ismoveRight)
            {
                transform.position = new Vector2(transform.position.x + moveSpeed, transform.position.y);

            }
            else
            {

                transform.position = new Vector2(transform.position.x + -moveSpeed, transform.position.y);
                transform.localScale = new Vector3(-2.957116f, 2.957116f, 2.957116f);

            }
            //destroy the game object when the timer is less than or equal to 0
            if (destroyTimer <= 0)
            {
                Destroy(gameObject);
            }

            destroyTimer -= Time.deltaTime;

        }
        //When any gameObject with a collider enters this objects collider
        void OnTriggerEnter2D(Collider2D other)
        {

            Debug.Log(damageToGive);

            //if the parent of the collider is called "Player" 
            if (other.name == "Player")
            {
                //Call the hurtPlayer method from the healthManager script
                HealthManager.HurtPlayer(damageToGive);
                //destroy the gameObject that this script is attached to
                Destroy(gameObject);

            }

        }
    }
}