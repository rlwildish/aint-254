﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{

    public class LifePickup : MonoBehaviour
    {

        private LifeManager lifeSystem;

        // Use this for initialization
        void Start()
        {
            lifeSystem = FindObjectOfType<LifeManager>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        //On collider enter give a life to the player and destroy the lifePickup object
        void OnTriggerEnter2D(Collider2D other)
        {
            lifeSystem.GiveLife();
            Destroy(gameObject);
        }
    }
}