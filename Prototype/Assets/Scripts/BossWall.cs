﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{

    public class BossWall : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //If you find a gameObject in the scene with this script on it, then exit the update loop. Otherwise destroy the gameObject this script is attached to
            if(FindObjectOfType<BossHealthManager>())
            {
                return;
            }

            Destroy(gameObject);

        }
    }
}