﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class BallBehaviour : MonoBehaviour
    {
        [SerializeField]
        private float speed = 0.05f;  //controls the speed the ball moves

        private MovingEntity m_ballMover;
        private Transform m_transform;


        // Use this for initialization
        void Start()
        {
            //we create a new entity
            m_ballMover = new MovingEntity();
            //we get a reference for transform element
            m_transform = GetComponent<Transform>();
        }

        // Update is called once per frame
        void Update()
        {
            m_ballMover.UpdateVelocity(new Vector3(1.0f, 0.0f, 0.0f) * speed * Time.deltaTime);
            m_ballMover.UpdateEntity();

            //trannsfer the position to unity so it can display it
            m_transform.position = m_ballMover.GetEntityPosition();
        }
    }
}
