﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartMenu : MonoBehaviour {

    public Canvas quitMenu;
    public Canvas controlsMenu;
    public Canvas optionsMenu;
    public Button playButton;
    public Button continueButton;
    public Button levelSelectButton;
    public Button optionsButton;
    public Button controlsButton;
    public Button quitButton;

    public int playerLives = 2;

	// Use this for initialization
	void Start () {

        quitMenu = quitMenu.GetComponent<Canvas>();
        controlsMenu = controlsMenu.GetComponent<Canvas>();
        optionsMenu = optionsMenu.GetComponent<Canvas>();
        playButton = playButton.GetComponent<Button>();
        continueButton = continueButton.GetComponent<Button>();
        levelSelectButton = levelSelectButton.GetComponent<Button>();
        controlsButton = controlsButton.GetComponent<Button>();
        optionsButton = optionsButton.GetComponent<Button>();
        quitButton = quitButton.GetComponent<Button>();

        quitMenu.enabled = false;
        controlsMenu.enabled = false;
        optionsMenu.enabled = false;
	
	}
	//enable or disable certain buttons and canvases based on buttons that are pressed
	public void EnterQuitSubMenu()
    {
        quitMenu.enabled = true;
        playButton.enabled = false;
        continueButton.enabled = false;
        levelSelectButton.enabled = false;
        controlsButton.enabled = false;
        optionsButton.enabled = false;
        quitButton.enabled = false;
    }

    public void EnterControlsSubMenu()
    {
        controlsMenu.enabled = true;
        playButton.enabled = false;
        continueButton.enabled = false;
        levelSelectButton.enabled = false;
        controlsButton.enabled = false;
        optionsButton.enabled = false;
        quitButton.enabled = false;
    }

    public void NoPress()
    {
        quitMenu.enabled = false;
        controlsMenu.enabled = false;
        optionsMenu.enabled = false;
        playButton.enabled = true;
        continueButton.enabled = true;
        levelSelectButton.enabled = true;
        controlsButton.enabled = true;
        optionsButton.enabled = true;
        quitButton.enabled = true;
    }
    //quit the application and return to desktop on button press this function is attached to
    public void QuitPress()
    {
        Application.Quit();
    }
    //set PlayerPrefs lives value to 2 on a new game
    public void Play()
    {
        PlayerPrefs.SetInt("PlayerCurrentLives", playerLives);
        Application.LoadLevel(3);
    }
    //if the PlayerPrefs value of currentLevel is more than 0 load that numbered level (levels are numbered based on build order)
    public void Continue()
    {
        if(PlayerPrefs.GetInt("CurrentLevel") <= 0)
        {

        }
        else
        {
            Application.LoadLevel(PlayerPrefs.GetInt("CurrentLevel"));
        }
    }
    //load level 3/levelSelect (index of 2 as they start at 0)
    public void LevelSelect()
    {
        Application.LoadLevel(2);
    }
}
