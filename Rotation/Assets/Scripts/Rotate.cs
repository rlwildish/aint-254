﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class Rotate : MonoBehaviour
    {
        [SerializeField]
        private float m_rotationSpeed = 5;
        private Transform m_transform;
        // Use this for initialization
        void Start()
        {
            m_transform = transform;
        }

        // Update is called once per frame
        void Update()
        {
            //m_transform.Rotate(new Vector3(0, 0, 1) * m_rotationSpeed * Time.deltaTime);
            //m_transform.Rotate(new Vector3(0, 0, 1) * m_rotationSpeed * Time.deltaTime, Space.Self);
            //m_transform.Rotate(new Vector3(0, 0, 1) * m_rotationSpeed * Time.deltaTime, Space.World);
            m_transform.RotateAround(new Vector3(3, 0, 0), new Vector3(0, 0, 1), 1.0f);

        }
    }
}