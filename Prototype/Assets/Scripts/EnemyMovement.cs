﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class EnemyMovement : MonoBehaviour
    {

        public float moveSpeed = 3;

        public bool moveRight;

        public Transform wallCheck;
        public float wallCheckRadius;
        public LayerMask whatIsWall;
        private bool isHittingWall;

        private bool isNotAtEdge;
        public Transform edgeCheck;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            //return true or false dependant on whether the position (with designated radius) is touching the other object (a specified layer in this case)
            isHittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
            //return true or false dependant on whether the position (with designated radius) is touching the other object (a specified layer in this case)
            isNotAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);
            //if these return true || false (respectively) change moveRights value to the the opposite (if true change to false and vice versa)
            if (isHittingWall || !isNotAtEdge)
            {
                moveRight = !moveRight;
            }
            //changes localScale to better represent movement in the game and gives the enmy specified movement dependant on the moveRight boolean value
            if (moveRight)
            {
                transform.localScale = new Vector3(1f, 1f, 1f);
                GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                transform.localScale = new Vector3(-1f, 1f, 1f);
                GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
            }

        }
    }
}