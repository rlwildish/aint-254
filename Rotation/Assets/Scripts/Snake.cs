﻿using UnityEngine;
using System.Collections;

public class Snake : MonoBehaviour {

    [SerializeField]
    private GameObject m_segment;

    private Transform[] m_snake;

    [SerializeField]
    private float m_amplitude = 0.6f;

    private float angleVel = 100.0f;

    private float m_angle;

    [SerializeField]
    [Range(1000.001f, 1100.001f)]
    private float m_period = 1010.0f;

    // Use this for initialization
	void Start () {
        CreateSnake();
	}
	
	// Update is called once per frame
	void Update () {

        for(int i = 0; i < m_snake.Length; i++)
        {
            float valueX = m_amplitude * Mathf.Sin(Mathf.PI * 2 * m_angle / m_period);
            m_snake[i].position = new Vector3(valueX, 0, m_snake[i].position.z);

            m_angle += angleVel;
        }
	
	}

    private Transform CreateSection()
    {
        GameObject _obj = Instantiate(m_segment);

        Transform tempTransform = _obj.GetComponent<Transform>();

        return tempTransform;
    }

    private void CreateSnake()
    {
        m_snake = new Transform[10];

        for(int i = 0; i < m_snake.Length; i++)
        {
            m_snake[i] = CreateSection();

            m_snake[i].position = new Vector3(0, 0, i * 0.25f);
        }
    }
}
