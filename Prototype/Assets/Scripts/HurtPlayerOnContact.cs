﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class HurtPlayerOnContact : MonoBehaviour
    {

        public int damageToGive = 1;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        //on trigger enter, if the object is the player hurt him, and give a knockback effect from the left or right, dependant on the transform of this object to the other collider (the player in all cases).
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                HealthManager.HurtPlayer(damageToGive);

                var player = other.GetComponent<PlayerBehaviour>();
                player.knockbackCount = player.knockbackLength;

                if (other.transform.position.x < transform.position.x)
                {
                    player.knockFromRight = true;
                }
                else
                {
                    player.knockFromRight = false;
                }
            }
        }
    }
}