﻿using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour {

    private int level;

	// Use this for initialization
	void Start () {

        level = Application.loadedLevel;
	
	}
	
	// Update is called once per frame
	void Update () {

        //Debug.Log(Application.loadedLevel);
	
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        //If the collision was from the player load this levelNumber but add 1 to it (essentially the next level)
        if (other.name == "Player")
        {
            
            Application.LoadLevel(level + 1);
        }
    }
}
