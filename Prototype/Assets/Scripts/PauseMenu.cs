﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour {

    public Canvas canvas;
    public Button resume;
    public Button controls;
    public Button options;
    public Button quitToMain;
    public Button quitToDesk;
    public int currentLevel;

	// Use this for initialization
	void Start () {

        canvas = canvas.GetComponent<Canvas>();
        resume = resume.GetComponent<Button>();
        controls = controls.GetComponent<Button>();
        options = options.GetComponent<Button>();
        quitToMain = quitToMain.GetComponent<Button>();
        quitToDesk = quitToDesk.GetComponent<Button>();
        currentLevel = Application.loadedLevel;
        canvas.enabled = false;

    }
	
	// Update is called once per frame
	void Update () {
	//If the player presses controls listed for Start (in input, for PC and controller)
        if(Input.GetButtonDown("Start"))
        {
            //if canvas is not enabled, enable it and set timeScale to 0 (freeze time)
            if (canvas.enabled == false)
            {
                canvas.enabled = true;
                Time.timeScale = 0;

            }
            else
            {
                //else disable the cavas and set timeScale to 1 (normal speed)
                canvas.enabled = false;
                Time.timeScale = 1;
            }
        }

	}
    //disable the cavas and set timeScale to 1 (normal speed)
    public void Resume()
    {
        canvas.enabled = false;
        Time.timeScale = 1;
    }

    public void Controls()
    {

    }

    public void Options()
    {

    }
    //Load the startMenu
    public void QuitToMain()
    {
        
        Time.timeScale = 1;
        PlayerPrefs.SetInt("CurrentLevel", currentLevel);
        Application.LoadLevel(0);
    }
    //quit the application and return to desktop
    public void QuitToDesk()
    {
        
        Time.timeScale = 1;
        PlayerPrefs.SetInt("CurrentLevel", currentLevel);
        Application.Quit();
    }
}
