﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class KillPlayer : MonoBehaviour
    {

        public LevelManager levelManager;
        private int deathPenalty = -100;

        // Use this for initialization
        void Start()
        {

            levelManager = FindObjectOfType<LevelManager>();


        }

        // Update is called once per frame
        void Update()
        {



        }
        //on entering collider if the other collider has a name of Player, respawn the player and deduct points from the players score
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.name == "Player")
            {
                levelManager.RespawnPlayer();
                UI.AddPoints(deathPenalty);
            }
        }
    }
}