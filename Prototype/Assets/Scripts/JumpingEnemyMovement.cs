﻿using UnityEngine;
using System.Collections;

public class JumpingEnemyMovement : MonoBehaviour
{

    public float moveSpeed = 5;

    public bool moveRight;

    private bool isGrounded;
    public Transform groundPoint;
    public float groundRadius = 0.01f;
    public Transform wallCheck;
    public float wallCheckRadius;
    public LayerMask whatIsWall;
    private bool isHittingWall;
    private Rigidbody2D jump;
    private float jumpTimer;

    private bool isNotAtEdge;
    public Transform edgeCheck;

    // Use this for initialization
    void Start()
    {
        jump = GetComponent<Rigidbody2D>();
        jumpTimer = 0;
        isNotAtEdge = true;
        isHittingWall = false;

    }

    // Update is called once per frame
    void Update()
    {

        //return true or false dependant on whether the position (with designated radius) is touching the other object (a specified layer in this case)
        isGrounded = Physics2D.OverlapCircle(groundPoint.position, groundRadius, whatIsWall);
        isHittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
        //if the enemy is grounded, check if is at edge, if is at an edge then change direction
        if (isGrounded)
        {
            isNotAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);

            if (!isNotAtEdge)
                moveRight = !moveRight;

        }
        //if the enemy is hitting a wall, change direction
        if (isHittingWall )
        {
            moveRight = !moveRight;
        }
        //movement and localScale
        if (moveRight)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            jump.velocity = new Vector2(moveSpeed, jump.velocity.y);
        }
        else
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            jump.velocity = new Vector2(-moveSpeed, jump.velocity.y);
        }

        jumpTimer -= Time.deltaTime;
        //jump if grounded and the jumpTimer is 0 or less, jumpTimer then set to 0.1 seconds to not allow the enemy to repeatedly jump (getting a higher jump)
        if (isGrounded && jumpTimer <= 0)
        {
            jump.AddForce(new Vector2(0, 500));

            jumpTimer = 0.1f;
        }



    }
    //draw a green circle at specified position and given radius, visible in unityEditor, invisible in build
    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(groundPoint.position, groundRadius);
    }

}