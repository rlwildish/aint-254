﻿using UnityEngine;
using System.Collections;

public class EnemyUpAndDownMovement : MonoBehaviour {

    public float moveSpeed = 3;

    public bool moveUp;

    public Transform topCheck;
    public Transform bottomCheck;
    public float wallCheckRadius = 0.1f;
    public LayerMask whatIsWall;
    private bool isHittingTopWall;
    private bool isHittingBottomWall;

    private float wallHitTimer = 1.0f;

    public Vector2 startPosition;

    // Use this for initialization
    void Start()
    {
        startPosition = new Vector2(GetComponent<Rigidbody2D>().position.x, GetComponent<Rigidbody2D>().position.y);
    }

    // Update is called once per frame
    void Update()
    {

        wallHitTimer -= Time.deltaTime;
        //return true or false dependant on whether the position (with designated radius) is touching the other object (a specified layer in this case)
        isHittingTopWall = Physics2D.OverlapCircle(topCheck.position, wallCheckRadius, whatIsWall);
        isHittingBottomWall = Physics2D.OverlapCircle(bottomCheck.position, wallCheckRadius, whatIsWall);


        if (!moveUp)
        {
            //if the position (y co-ordinate) of this object is less than the startPosition - 7 start moving the other direction
            if (GetComponent<Rigidbody2D>().position.y <= startPosition.y - 7)
            {
                moveUp = !moveUp;
            }
        }

        if (moveUp)
        {
            //if the position (y co-ordinate) of this object is more than the startPosition + 7 start moving the other direction
            if (GetComponent<Rigidbody2D>().position.y >= startPosition.y + 7)
            {
                moveUp = !moveUp;
            }
        }
        //timer for wallHit allows for the enemy to not get stuck in the wall continually switching between false and true for moveUp (this may be better suited in a FixedUpdate loop to avoid extra timer/code)
        if (wallHitTimer <= 0)
        {
            if (isHittingTopWall)
            {
                moveUp = !moveUp;
                wallHitTimer = 1.0f;
            }

            if (isHittingBottomWall)
            {
                moveUp = !moveUp;
                wallHitTimer = 1.0f;
            }
        }

        
        //give this enemy movement based on the moveUp boolean value
        if (moveUp)
        {
            
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, moveSpeed);
        }
        else
        {
           
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -moveSpeed);
        }

    }
}
