﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	void OnGUI()
    {
        GUI.Label(new Rect(5.0f, 5.0f, 200.0f, 25.0f), "Left X-Axis: ");
        GUI.Label(new Rect(155.0f, 5.0f, 200.0f, 25.0f), Input.GetAxis("Horizontal").ToString());
        GUI.Label(new Rect(5.0f, 25.0f, 200.0f, 25.0f), "Left Y-Axis: ");
        GUI.Label(new Rect(155.0f, 25.0f, 200.0f, 25.0f), Input.GetAxis("Vertical").ToString());

        //GUI.Label(new Rect(5.0f, 5.0f, 200.0f, 25.0f), "Left Mouse Button: ");
        //GUI.Label(new Rect(155.0f, 5.0f, 200.0f, 25.0f), Input.GetButton(KeyCode.));

        GUI.Label(new Rect(5.0f, 45.0f, 200.0f, 25.0f), "Right X-Axis: ");
        GUI.Label(new Rect(155.0f, 45.0f, 200.0f, 25.0f), Input.GetAxis("RightX").ToString());
        GUI.Label(new Rect(5.0f, 65.0f, 200.0f, 25.0f), "Right Y-Axis: ");
        GUI.Label(new Rect(155.0f, 65.0f, 200.0f, 25.0f), Input.GetAxis("RightY").ToString());

        GUI.Label(new Rect(5.0f, 85.0f, 200.0f, 25.0f), "Left Trigger: ");
        GUI.Label(new Rect(155.0f, 85.0f, 200.0f, 25.0f), Input.GetAxis("LeftTrigger").ToString());
        GUI.Label(new Rect(5.0f, 105.0f, 200.0f, 25.0f), "Right Trigger: ");
        GUI.Label(new Rect(155.0f, 105.0f, 200.0f, 25.0f), Input.GetAxis("RightTrigger").ToString());

        GUI.Label(new Rect(5.0f, 125.0f, 200.0f, 25.0f), "Left D-Pad: ");
        GUI.Label(new Rect(155.0f, 125.0f, 200.0f, 25.0f), Input.GetAxis("LeftD-Pad").ToString());
        GUI.Label(new Rect(5.0f, 145.0f, 200.0f, 25.0f), "Right D-Pad: ");
        GUI.Label(new Rect(155.0f, 145.0f, 200.0f, 25.0f), Input.GetAxis("RightD-Pad").ToString());

        if (Mathf.Abs(Input.GetAxis("Horizontal")) < 0.25f)
        {

        }

    }
}
