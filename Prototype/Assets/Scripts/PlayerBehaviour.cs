﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class PlayerBehaviour : MonoBehaviour
    {

        private float moveSpeed;
        
        private int jumpHeight;
        private int orientation;

        public Transform groundPoint;
        public Transform groundPoint2;
        public float radius;
        public LayerMask groundMask;
        private Transform trans;
        public bool isGrounded;
        public bool isGrounded2;
        public static bool isScooter;
        //bool isPogo;
        public Rigidbody2D rb2D;

        private float autoJumpTimer = 0.0f;

        private bool hasPressedJump = false;

        public float knockback = 30;
        public float knockbackLength = 0.2f;
        public float knockbackCount;
        public bool knockFromRight;

        void Start()
        {
            trans = GetComponent<Transform>();
            Debug.Log(trans);
            rb2D = GetComponent<Rigidbody2D>();
            isScooter = true;
            //isPogo = false;
            moveSpeed = 15;
            jumpHeight = 300;
        }

        void Update()
        {
            //set moveDIr to any Horizontal input (listed in Input, with PC and controller support)
            Vector2 moveDir = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, rb2D.velocity.y);
            rb2D.velocity = moveDir;

            isGrounded = Physics2D.OverlapCircle(groundPoint.position, radius, groundMask);
            isGrounded2 = Physics2D.OverlapCircle(groundPoint2.position, radius, groundMask);

            if (knockbackCount <= 0)
            {
                //If player uses any Input listed in Switch Input on buttonDown
                if (Input.GetButtonDown("Switch"))
                {
                    //change isScooter to opposite value, change localScale to represent change in mode
                    isScooter = !isScooter;

                    if (isScooter == true)
                    {
                        trans.localScale = new Vector3(1, 1f, 1);
                    }
                    else
                    {
                        trans.localScale = new Vector3(1, -1f, 1);
                        autoJumpTimer = 0.0f;
                    }
                }
                //set movement variables
                if (isScooter == true)
                {
                    moveSpeed = 20;
                    jumpHeight = 1200;
                }
                //set movement variables and make the character auto jump in pogo mode if timer is 0 or less and the player hasn't pressed space
                if (isScooter == false)
                {
                    moveSpeed = 10;
                    jumpHeight = 2200;

                    if (isGrounded2)
                    {

                        if(autoJumpTimer < 0)
                        {
                            hasPressedJump = false;
                        }

                        autoJumpTimer -= Time.deltaTime;

                        if (autoJumpTimer <= 0 && !hasPressedJump)
                        {
                            
                            rb2D.AddForce(new Vector2(0, (jumpHeight / 2)));
                            
                            autoJumpTimer = 0.2f;
                        }
                    }
                }
                //if Horizontal input is more than or equal to 0.1 change character localScale (allows for dead input, where controller sticks can be uncentred)
                if (Input.GetAxisRaw("Horizontal") >= 0.1f)
                {
                    if (isScooter == true)
                    {
                        trans.localScale = new Vector3(1, 1f, 1);
                    }
                    else if (isScooter == false)
                    {
                        trans.localScale = new Vector3(1, -1f, 1);
                    }
                }
                //if Horizontal input is less than or equal to -0.1 change character localScale(allows for dead input, where controller sticks can be uncentred)
                else if (Input.GetAxisRaw("Horizontal") <= -0.1f)
                {
                    if (isScooter == true)
                    {
                        trans.localScale = new Vector3(-1, 1f, 1);
                    }
                    else if (isScooter == false)
                    {
                        trans.localScale = new Vector3(-1, -1f, 1);
                    }
                }
                //if timeScale is more than 0 and the player has used an input from Jump and is grounded from eitherof the groundChecks make the player jump and set hasPressedJump to true
                if(Time.timeScale > 0)
                if (Input.GetButtonDown("Jump") && isGrounded || Input.GetButtonDown("Jump") && isGrounded2)
                {
                    rb2D.AddForce(new Vector2(0, jumpHeight));
                    hasPressedJump = true;
                }
            }
            else
            {
                //knock player left or right dependant on knockFromRight value
                if (knockFromRight)
                {
                    rb2D.velocity = new Vector2(-knockback, knockback);
                }
                if (!knockFromRight)
                {
                    rb2D.velocity = new Vector2(knockback, knockback);
                }
                knockbackCount -= Time.deltaTime;
            }
        }
        //create circle to detect groundChecks, not visible in actual build
        void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(groundPoint.position, radius);
            Gizmos.DrawWireSphere(groundPoint2.position, radius);
        }
        //disables the player, so you can't see the player, but keeps the object in the scene so scripts can manipulate the object
        public void DisablePlayer()
        {
            gameObject.SetActive(false);
        }

        public void EnablePlayer()
        {
            gameObject.SetActive(true);
        }
        //makes sure to reset the player to scooter mode on respawn so the player doesn't have to remember which mode they were in, they will always expect to be in scooter mode on respawn
        public void SwitchPlayerModeOnRespawn()
        {
            isScooter = true;
        }
        //a static method that can be called from any script to allow them to know if the player is in scooter mode or not
        public static bool GetIsScooter()
        {
            return isScooter;
        }
    }
}