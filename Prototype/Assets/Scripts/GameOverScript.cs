﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverScript : MonoBehaviour {

    public Button mainMenu;
    public Button desktop;


	// Use this for initialization
	void Start () {
        //get the button compnent for these objects
        mainMenu = mainMenu.GetComponent<Button>();
        desktop = desktop.GetComponent<Button>();

	}
	
	// Update is called once per frame
	void Update () {
	


	}
    //load level 0 in build order
    public void MainMenu()
    {
        Application.LoadLevel(0);
    }
    //quit application and return to desktop
    public void Desktop()
    {
        Application.Quit();
    }
}
