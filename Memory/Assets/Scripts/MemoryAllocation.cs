﻿using UnityEngine;
using System.Collections;

public class MemoryAllocation : MonoBehaviour
{

    private Transform m_transform;

    private GameObject[] m_transArray;

    private string testString1;
    private string testString2;

    private int hashString1;
    private int hashString2;

	// Use this for initialization
	void Start ()
    {
        hashString1 = testString1.GetHashCode();
        hashString2 = testString2.GetHashCode();

	    m_transform = transform;

        m_transArray = new GameObject[1000];

        for(int i = 0; i < m_transArray.Length; i++)
        {
            GameObject obj = Instantiate(gameObject);
            m_transArray[i] = obj;
            m_transArray[i].SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {

        //TestTransform();
        //TestTransformOptimized();
        //ObjectAllocation();
        //TestInstantiate();
        TestArrayInstantiate();

	}

    private void TestTransform()
    {

        for(int i = 0; i < 10000; i++)
        {
            transform.position = Vector3.zero;
        }

    }

    private void TestTransformOptimized()
    {

        for (int i = 0; i < 10000; i++)
        {
            m_transform.position = Vector3.zero;
        }

    }

    private void ObjectAllocation()
    {

        int test = 1;
        int test2 = test;

        for (int i = 0; i < 1000; i++)
        {
            TestObject customObj = new TestObject();
        }

    }

    private void TestInstantiate()
    {
        GameObject obj = Instantiate(gameObject);
        obj.transform.position = new Vector3(1, 0, 0);
        Destroy(obj);
    }

    private void TestArrayInstantiate()
    {

        for(int i = 0; i < 1000; i++)
        {
            m_transArray[i].SetActive(true);
            m_transArray[i].transform.position = new Vector3(1, 0, 0);
            m_transArray[i].SetActive(false);
        }

    }

    private float[] RandomArray(int numberOfElements)
    {
        var results = new float[numberOfElements];

        for (int i = 0; i < results.Length; i++)
        {
            results[i] = Random.value;
        }

        return results;
    }

    private void RandomArrayOptimized(float[] arrayToFill)
    {

        for (int i = 0; i < arrayToFill.Length; i++)
        {
            arrayToFill[i] = Random.value;
        }

    }

    private bool CompareTwoStrings(string stringOne, string stringTwo)
    {

        if(stringOne == stringTwo)
            return true;
        
        return false;

    }

    private bool CompareTwoInts(int intOne, int intTwo)
    {
        if (intOne == intTwo)
            return true;

        return false;
    }

}
