﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    //this script is almost exactly the same as the BossBullet script but uses the ShootyEnemy Getter method instead to work out which direction it should travel.
    //Not commented due to duplicated script BossBullet
    public class Bullet : MonoBehaviour
    {

        public static bool ismoveRight;
        private float moveSpeed = 0.5f;
        private float destroyTimer = 2f;
        private static int damageToGive = 1;
        

        public Vector2 playerPos;

        // Use this for initialization
        void Start()
        {

            ismoveRight = ShootyEnemy.GetMoveRight();

        }

        // Update is called once per frame
        void Update()
        {



            if (ismoveRight)
            {
                transform.position = new Vector2(transform.position.x + moveSpeed, transform.position.y);

            }
            else
            {

                transform.position = new Vector2(transform.position.x + -moveSpeed, transform.position.y);
                transform.localScale = new Vector3(-2.957116f, 2.957116f, 2.957116f);

            }

            if (destroyTimer <= 0)
            {
                Destroy(gameObject);
            }

            destroyTimer -= Time.deltaTime;

        }

        void OnTriggerEnter2D(Collider2D other)
        {
            
            Debug.Log(damageToGive);
            
            
                if (other.name == "Player")
                {
                    
                    HealthManager.HurtPlayer(damageToGive);
                    Destroy(gameObject);

                }

                if(other.name == "Wall 1")
            {
                Destroy(gameObject);
            }
            
        }
    }
}