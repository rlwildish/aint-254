﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RyanWildish
{

    public class TimeManager : MonoBehaviour
    {

        public float levelTimer;

        public Text timeText;

        public Canvas outOfTime;

        public float failedTimer = 3f;

        private LevelManager levelManager;

        private LifeManager lifeManager;

        private bool lifeTaken = false;

        // Use this for initialization
        void Start()
        {

            timeText = timeText.GetComponent<Text>();

            outOfTime = outOfTime.GetComponent<Canvas>();

            outOfTime.enabled = false;

            levelManager = FindObjectOfType<LevelManager>();

            lifeManager = FindObjectOfType<LifeManager>();

        }

        // Update is called once per frame
        void Update()
        {

            levelTimer -= Time.deltaTime;
            //set the float value to an rounded up int (if value of float was 47.5768493749309, it would change the value to 48. This shows the user a more logical and pleasant to look at timer
            timeText.text = "Time: " + Mathf.Round(levelTimer);
            //If the player runs out of time for the level, start the failedTimer and respawn the player (so they can't move) and then when failedTimer is 0 or less reload the level and take a life away from the player
            if (levelTimer < 0)
            {

                failedTimer -= Time.deltaTime;
                levelManager.RespawnPlayer();
                
                outOfTime.enabled = true;

                if(failedTimer <= 0)
                {
                    

                    Application.LoadLevel(PlayerPrefs.GetInt("CurrentLevel"));

                    //This part takes 2 lives for some reason so give the player a life back and only 1 is taken in the end, don't quite know what is occuring here, but at least it works
                    lifeManager.TakeLife();
                         
                    lifeManager.GiveLife();
                    
                }
            }

        }
    }
}