﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{

    public class HurtPlayerBullet : MonoBehaviour
    {

        public bool damaged = false;

        public int damageToGive = 1;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        //on enter collider if the bullet hasn't damaged the player and the object is the player, hurt the player, give the player a knockback (from left or right) and set damaged to true
        void OnTriggerEnter2D(Collider2D other)
        {
            if(!damaged)
            if (other.name == "Player")
            {
                HealthManager.HurtPlayer(damageToGive);

                var player = other.GetComponent<PlayerBehaviour>();
                player.knockbackCount = player.knockbackLength;

                if (other.transform.position.x < transform.position.x)
                {
                    player.knockFromRight = true;
                }
                else
                {
                    player.knockFromRight = false;
                }
                damaged = true;
            }
            
        }
    }
}
