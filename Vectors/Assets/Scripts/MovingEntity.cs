﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class MovingEntity
    {
        private Vector3 m_positionEntity;
        private Vector3 m_velocityEntity;

        /// <summary>
        /// constructor
        /// </summary>
        public MovingEntity()
        {
            //here we give our moving entity some default values
            m_positionEntity = new Vector3(0.0f, 0.0f, 0.0f);
            m_velocityEntity = Vector3.zero; //same thing like - new Vector3(0.0f, 0.0f, 0.0f);
        }

        /// <summary>
        /// updates the entity position - private(we can access it only from inside this class
        /// </summary>
        private void UpdatePosition()
        {
            m_positionEntity = m_positionEntity + m_velocityEntity;
        }

        /// <summary>
        /// updates enitity's velocity 
        /// </summary>
        /// <param name="_vector"></param>
        public void UpdateVelocity(Vector3 _vector)
        {
            m_velocityEntity = _vector;
        }

        /// <summary>
        /// we call this from the update method in BallBehaviour- driven by unity(called every frame)
        /// </summary>
        public void UpdateEntity()
        {
            UpdatePosition();
        }

        /// <summary>
        /// just returns the entity position so Unity can display it for us
        /// </summary>
        /// <returns> vector3 - entity's position</returns>
        public Vector3 GetEntityPosition()
        {
            return m_positionEntity;
        }
    }
}
