﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class Checkpoint : MonoBehaviour
    {

        public LevelManager levelManager;

        void Start()
        {
            //Searching the scene for any active objects with the LevelManager script
            levelManager = FindObjectOfType<LevelManager>();
        }

        void Update()
        {

        }
        //On entering collision if it's the player change the current checkpoint to this one
        void OnTriggerEnter2D(Collider2D other)
        {

            if (other.name == "Player")
            {
                levelManager.currentCheckpoint = gameObject;
                Debug.Log("new checkpoint");
            }

        }
    }
}