﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class LevelManager : MonoBehaviour
    {

        public GameObject currentCheckpoint;

        private PlayerBehaviour player;

        private EnemyHealhtManager enemies;

        public GameObject deathParticle;

        public GameObject respawnParticle;

        public float respawnDelay = 3.0f;

        private CameraFollow2D cam;

        public HealthManager healthManager;

        private int currentLevel;

        // Use this for initialization
        void Start()
        {
            //if the PlayerPrefs variable CurrentLives is less than 0 set the value to 2
            //This ensures that the player has 3 lives (game over on -1 lives) 
            //Only happens at the start of the level so if the player reaches -1 lives during the level this won't trigger
            if (PlayerPrefs.GetInt("CurrentLives") < 0)
            {
                PlayerPrefs.SetInt("CurrentLives", 2);
            }

            player = FindObjectOfType<PlayerBehaviour>();

            cam = FindObjectOfType<CameraFollow2D>();

            healthManager = FindObjectOfType<HealthManager>();

            enemies = FindObjectOfType<EnemyHealhtManager>();

            currentLevel = Application.loadedLevel;

            PlayerPrefs.SetInt("CurrentLevel", currentLevel);
        }
        //call function as coroutine, has "virtually no perfomance loss", 
        public void RespawnPlayer()
        {

            StartCoroutine("RespawnPlayerCo");

        }

        public IEnumerator RespawnPlayerCo()
        {
            //Instantiate prefab at position and rotation, playerHealth = 0, disable the player, stop the camera following player, set players gravityScale to 0
            Instantiate(deathParticle, player.transform.position, player.transform.rotation);
            HealthManager.playerHealth = 0;
            player.DisablePlayer();
            cam.isFollowing = false;
            player.GetComponent<Rigidbody2D>().gravityScale = 0f;

            Debug.Log("Player Respawn");

           player.SwitchPlayerModeOnRespawn();
            //This pauses the function and specifies when the coroutine should continue, it will wait for respawnDelay amount of seconds, without "hogging" the CPU.
            yield return new WaitForSeconds(respawnDelay);

            //move the player to the position of the currentCheckpoint, knockbackCount = 0 (so any remaining knockback is nullified), enable the player, give player fullHealth and set isDead to false, make the camera follow the player and set gravityScale of player to 5.
            player.transform.position = currentCheckpoint.transform.position;
            player.knockbackCount = 0;

            player.EnablePlayer();
            healthManager.FullHealth();
            healthManager.isDead = false;
            cam.isFollowing = true;
            player.GetComponent<Rigidbody2D>().gravityScale = 5f;
            //Instantiate a respawn particle
            Instantiate(respawnParticle, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
        }
    }
}