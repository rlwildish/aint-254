﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class CoinScript : MonoBehaviour
    {

        public int pointsToAdd = 10;

        //On entering this collider
        void OnTriggerEnter2D(Collider2D other)
        {
            //if the object doesn't have a playerBehaviour script exit this loop
            if (other.GetComponent<PlayerBehaviour>() == null)
            {
                return;
            }
            //otherwise give the player some points and destroy the coin
            UI.AddPoints(pointsToAdd);

            Destroy(gameObject);

        }
    }
}