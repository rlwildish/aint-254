﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class Move : MonoBehaviour
    {

        [SerializeField]
        private float m_speed = 5.0f;

        private Rigidbody m_rigidbody;
        // Use this for initialization
        void Start()
        {
            m_rigidbody = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if(Input.GetAxis("Vertical") > 0)
            {
                m_rigidbody.AddForce(transform.forward * m_speed * Time.deltaTime);
            }

            
        }
    }
}