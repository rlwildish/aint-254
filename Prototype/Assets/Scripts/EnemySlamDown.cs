﻿using UnityEngine;
using System.Collections;
namespace ISS
{
    public class EnemySlamDown : MonoBehaviour {

        private float moveSpeed = 60;

        private float slamTimer = 10.0f;
        private float upTimer = 2.0f;

        public GameObject player;

        private Vector2 playerPosition;
        private Vector2 startPosition;

        public Transform upCheck;
        public Transform downCheck;
        public float wallCheckRadius = 0.1f;
        public LayerMask whatIsWall;
        private bool isHittingDownWall;
        private bool isHittingUpWall;

        private bool moveDown = false;
        

        // Use this for initialization
        void Start() {

           

            GetComponent<Rigidbody2D>().gravityScale = 0;

            moveDown = false;

        }

        // Update is called once per frame
        void Update() {
            //updates the variable to contain the position of the player every tick: this allows for the enemy to know if the player is within it's range (may have to put in a FixedUpdate loop if performance is hit too hard by having too many of this enemy on one level)
            playerPosition = player.GetComponent<Rigidbody2D>().position;
            //return true or false dependant on whether the position (with designated radius) is touching the other object (a specified layer in this case)
            isHittingDownWall = Physics2D.OverlapCircle(downCheck.position, wallCheckRadius, whatIsWall);
            isHittingUpWall = Physics2D.OverlapCircle(upCheck.position, wallCheckRadius, whatIsWall);

            slamTimer -= Time.deltaTime;
            //if moveDown returns true set this objects gravityScale to 1 also give this enemy the specified movement
            if (moveDown == true && GetComponent<Rigidbody2D>().gravityScale == 1)
            {

                GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -moveSpeed);
                
            }
            //if isHittingDownWall returns true, set moveDown to false and start decreasing the upTimer
            if (isHittingDownWall)
            {
                moveDown = false;
                upTimer -= Time.deltaTime;
            }
            //if the upTimer is less than or eqwual to 0 and moveDown is false start moving back up
            //when this enemy hits a wall above it set the gravityScale of this enemy to 0, reset upTimer and set moveDown to false
            if (upTimer <= 0 && moveDown == false)
            {
                
                    GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, moveSpeed);

                    if (isHittingUpWall)
                    {
                        
                        GetComponent<Rigidbody2D>().gravityScale = 0;
                        
                       
                        moveDown = true;
                        upTimer = 2;
                    }
               
            }
            //if the slamTimer is less than or equal to 10 and the player is within a 10 radius range on the x axis, make this enemy slam down
            if (slamTimer <= 0)
            {
                
                if (GetComponent<Rigidbody2D>().position.x - 10 <= player.GetComponent<Rigidbody2D>().position.x && GetComponent<Rigidbody2D>().position.x + 10 >= player.GetComponent<Rigidbody2D>().position.x)
                {
                    
                    
                    moveDown = true;
                    GetComponent<Rigidbody2D>().gravityScale = 1;
                    
                    slamTimer = 5;
                    
                }
            }

        }

       
    }
}
