﻿using UnityEngine;
using System.Collections;

namespace ISS
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_dot;

        private GameObject[] m_line;

        [SerializeField]
        private float m_force = 5;

        Vector3 tempDirection = Vector3.zero;

        // Use this for initialization
        void Start()
        {
            m_line = new GameObject[10];

            for (int i = 0; i < 10; i++)
            {

                GameObject tempGameObj = Instantiate(m_dot, Vector3.zero, Quaternion.identity) as GameObject;

                tempGameObj.SetActive(false);

                m_line[i] = tempGameObj;

            }

        }

        // Update is called once per frame
        void Update()
        {
            

            if (Input.GetMouseButton(0))
            {
                Vector3 tempPosition = Camera.main.WorldToScreenPoint(transform.position);
                tempPosition.z = 0;

                tempDirection = (Input.mousePosition - tempPosition).normalized;

                Aim(tempDirection);
            }

            if(Input.GetMouseButtonUp(0))
            {
                transform.GetComponent<Rigidbody>().AddForce(tempDirection * m_force * -10, ForceMode.Impulse);

                for (int i = 0; i < m_line.Length; i++)
                {

                    m_line[i].SetActive(false);

                }



            }
        }

        private void Aim(Vector3 _direction)
        {

            float Vox = _direction.x * m_force;
            float Voy = _direction.y * m_force;

            for(int i = 0; i < m_line.Length; i++)
            {
                float t = i * 0.1f;

                m_line[i].transform.position = new Vector3(transform.position.x - Vox * t,
                    transform.position.y - (Voy * t )- (0.5f * 9.8f * t * t), 0.0f);

                m_line[i].SetActive(true);

            }

        }
    }
}