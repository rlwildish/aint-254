﻿using UnityEngine;
using System.Collections;

namespace RyanWildish
{
    public class MoveBehaiviour : MonoBehaviour
    {
        private Transform m_transform;

        private int[] m_array;
        public AnimationCurve m_anim;
        // Use this for initialization
        void Start()
        {
            m_transform = GetComponent<Transform>();
            m_array = new int[5];

            m_array[0] = 0;
            m_array[1] = 0;
            m_array[2] = 1;
            m_array[3] = 2;
            m_array[4] = 3;
        }

        // Update is called once per frame
        void Update()
        {
            //Step();
            //StepArray();
            //StepPercentage();
            //StepAnimCurve();
            //StepVector();
            PerlinNoise();
        }
            private void Step()
        {
            int randomRange = Random.Range(0, 5);
            Debug.Log(randomRange);

            float valX = 0;
            float valZ = 0;

            if (randomRange == 0)
            {
                valX = 0.1f;
            }
            else if (randomRange == 1)
            {
                valX = -0.1f;
            }
            else if (randomRange == 2)
            {
                valZ = 0.1f;
            }
            else
            {
                valZ = -0.1f;
            }

            m_transform.position += new Vector3(valX, 0.0f, valZ);
        }
        private void StepArray()
        {
            int randomRange = Random.Range(0, 5);
            Debug.Log(randomRange);

            int temp = m_array[randomRange];

            float valX = 0;
            float valZ = 0;


            if (temp == 0)
            {
                valX = 0.1f;
            }
            else if (temp == 1)
            {
                valX = -0.1f;
            }
            else if (temp == 2)
            {
                valZ = 0.1f;
            }
            else
            {
                valZ = -0.1f;
            }

            m_transform.position += new Vector3(valX, 0.0f, valZ);
        }

        private void StepPercentage()
        {

            float valX = 0;
            float valZ = 0;

            float randomRange = Random.Range(0.0f, 1.0f);

            if(randomRange < 0.25f)
            {
                valX = 0.1f;
            }
            else if(randomRange < 0.50f)
            {
                valX = -0.1f;
            }

            else if(randomRange < 0.75f)
            {
                valZ = 0.1f;
            }
            else
            {
                valZ = -0.1f;
            }

            Debug.Log(randomRange);

            m_transform.position += new Vector3(valX, 0.0f, valZ);


        } 

        private void StepAnimCurve()
        {
            float randomCurve = Random.Range(0.0f, 1.0f);
            float valX = 0;
            float valZ = 0;

            float randomRange = m_anim.Evaluate(randomCurve);

            if (randomRange < 0.25f)
            {
                valX = 0.1f;
            }
            else if (randomRange < 0.50f)
            {
                valX = -0.1f;
            }

            else if (randomRange < 0.75f)
            {
                valZ = 0.1f;
            }
            else
            {
                valZ = -0.1f;
            }

            Debug.Log(randomRange);

            m_transform.position += new Vector3(valX, 0.0f, valZ);

        }

        private void StepVector()
        {
            float valX = Random.Range(-0.1f, 0.1f);
            float valZ = Random.Range(-0.1f, 0.1f);
            m_transform.position += new Vector3(valX, 0.0f, valZ);

        }

        private void PerlinNoise()
        {
            float valX = Mathf.PerlinNoise(Time.time, 0.0f);
            float valZ = Mathf.PerlinNoise(0.0f, Time.time);
            m_transform.position += new Vector3(valX, 0.0f, valZ);
        }


    }
}