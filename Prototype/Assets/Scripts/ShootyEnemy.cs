﻿using UnityEngine;
using System.Collections;

public class ShootyEnemy : MonoBehaviour {

    public float moveSpeed = 3;

    public static bool moveRight;

    public Transform wallCheck;
    public float wallCheckRadius;
    public LayerMask whatIsWall;
    private bool isHittingWall;

    private bool isNotAtEdge;
    public Transform edgeCheck;

    public GameObject bullet;
    public float shootTimer = 3;
    public GameObject gunBarrel;

    public GameObject player;

    public Vector2 playerPos;



    // Update is called once per frame
    void Update()
    {
        //playerPos is set to player.transform, only need x and y values as game is 2D
        playerPos = new Vector2(player.transform.position.x, player.transform.position.y);

        isHittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);

        isNotAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);
        //allow the enemy to not continually run into a wall or off of an edge
        if (isHittingWall || !isNotAtEdge)
        {
            moveRight = !moveRight;
        }
        //movement and localScale dependant on value of moveRight(which way he should be going)
        if (moveRight)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        //if the shootTimer is 0 or less and the player is within a 50 radius (on the x axis) call the shoot function and set the shootTimer to a random value between 1 and 5
        if (shootTimer <= 0 && GetComponent<Rigidbody2D>().position.x - 50 <= playerPos.x && GetComponent<Rigidbody2D>().position.x + 50 >= playerPos.x)
        {
            
            Shoot();
            shootTimer = Random.Range(1, 5);
        }
        
        //this makes the enemy always face towards the player and is separated from the above bit of code so he wont wait until he can shoot before facing the player, moveSpeed set to 0 so he can't move (as if he was taking aim)
        if (GetComponent<Rigidbody2D>().position.x - 50 <= playerPos.x && GetComponent<Rigidbody2D>().position.x + 50 >= playerPos.x)
        {
            if (GetComponent<Rigidbody2D>().position.x - 50 <= playerPos.x && GetComponent<Rigidbody2D>().position.x >= playerPos.x)
            {
                moveRight = false;
                transform.localScale = new Vector3(-1f, 1f, 1f);
                moveSpeed = 0;
            }
            else
            {
                moveRight = true;
                transform.localScale = new Vector3(1f, 1f, 1f);
                moveSpeed = 0;
            }
         }
        else
        {
            moveSpeed = 3;
        }


        shootTimer -= Time.deltaTime;

    }
    //Instantiate a bullet at specified position and rotation
    private void Shoot()
    {

        Instantiate(bullet, gunBarrel.transform.position, gunBarrel.transform.rotation);    

    }
    //a static Getter function so the bullet will be able to get this value and know whether it should travel left or right
    public static bool GetMoveRight()
    {

        return moveRight;

    }
}
