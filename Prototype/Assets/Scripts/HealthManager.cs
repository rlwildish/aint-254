﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


    namespace RyanWildish
    {
        public class HealthManager : MonoBehaviour
        {

            public static int playerHealth;

            public int maxPlayerHealth;

            Text text;

            private LevelManager levelManager;

            public bool isDead;

            private static float damageTimer = 1;

            private static bool damaged = false;

            private LifeManager lifeManager;

            

            // Use this for initialization
            void Start()
            {

                text = GetComponent<Text>();

                playerHealth = maxPlayerHealth;

                levelManager = FindObjectOfType<LevelManager>();

                isDead = false;

                lifeManager = FindObjectOfType<LifeManager>();

            }

            // Update is called once per frame
            void Update()
            {
            //if the playerHealth is 0 or less and the player is not dead
                if (playerHealth <= 0 && !isDead)
                {
                //set health to 0, respawn the player, set is dead to true and take a life from the lifeCount
                    playerHealth = 0;
                    levelManager.RespawnPlayer();
                    isDead = true;
                    lifeManager.TakeLife();
                }
                //update health value for the UI/HUD
                text.text = "Health: " + playerHealth;
            //if damageTimer is 0 or less set damaged to false
                if(damageTimer <= 0)
                {
                    damaged = false;
                }

            damageTimer -= Time.deltaTime;

            }
        //if the damaged returns false allow the player to be damaged and set damaged to true and the timer to 1
            public static void HurtPlayer(int damageToGive)
            {
                if (!damaged)
                {
                    playerHealth -= damageToGive;
                    damaged = true;
                    damageTimer = 1;
                }
            }
        //set the players health to the maximum value (planned to be used with a pickup)
            public void FullHealth()
            {
                playerHealth = maxPlayerHealth;
            }
        }
    }

